CREATE DATABASE BlackSoundDB

USE BlackSoundDB

CREATE TABLE Users(
	[UserId] INT PRIMARY KEY IDENTITY NOT NULL,
	Email VARCHAR(80) UNIQUE NOT NULL,
	[Name] NVARCHAR(80) UNIQUE NOT NULL,
	Is_admin BIT DEFAULT 0 NOT NULL,
)

CREATE TABLE Playlists(
	PlaylistId INT PRIMARY KEY IDENTITY NOT NULL,
	[Name] VARCHAR(80) NOT NULL,
	IsPublic BIT DEFAULT 0 NOT NULL,
)

CREATE TABLE Songs(
	SongId INT PRIMARY KEY IDENTITY NOT NULL,
	Title VARCHAR(100) NOT NULL,
	Author VARCHAR(120) NOT NULL,
	[Year] CHAR(4)
)

CREATE TABLE SongPlaylists(
	PlaylistId INT,
	SongId INT,
	CONSTRAINT PK_SongPlaylists
	PRIMARY KEY (PlaylistId , SongId),
	CONSTRAINT FK_SongPlaylists_Songs
	FOREIGN KEY (SongId) REFERENCES Songs(SongId),
	CONSTRAINT FK_SongPlaylists_Playlists
	FOREIGN KEY (PlaylistId) REFERENCES Playlists(PlaylistId)
)

CREATE TABLE UserPlaylists(
	PlaylistId INT,
	[UserId] INT,
	CONSTRAINT PK_UserPlaylists
	PRIMARY KEY (PlaylistId , [UserId]),
	CONSTRAINT FK_UserPlaylists_Users
	FOREIGN KEY ([UserId]) REFERENCES Users([UserId]),
	CONSTRAINT FK_UserPlaylists_Playlists
	FOREIGN KEY (PlaylistId) REFERENCES Playlists(PlaylistId)
)