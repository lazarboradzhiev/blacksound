﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace BlackSound.Data.Repositories
{
    internal static class Utility
    {
        internal static SqlCommand AddParameter(
            this SqlCommand command,
            string parameterName, 
            object parameterValue)
        {
            SqlParameter parameter = command.CreateParameter();
            parameter.ParameterName = parameterName;
            parameter.Value = parameterValue;

            command.Parameters.Add(parameter);
            return command;
        }
    }
}
