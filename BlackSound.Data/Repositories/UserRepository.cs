﻿using BlackSound.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace BlackSound.Data.Repositories
{
    public class UserRepository
    {
        private readonly string connectionString;

        public UserRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<User> GetAll()
        {
            List<User> users = new List<User>();

            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM Users";
                SqlDataReader reader = command.ExecuteReader();

               

                using (reader)
                {
                    while (reader.Read())
                    {
                        User user = new User();
                        user.Id = (int)reader["UserId"];
                        user.Name = reader["Name"].ToString();
                        user.Email = reader["Email"].ToString();
                        user.IsAdmin = (bool)reader["IsAdmin"];

                        users.Add(user);
                    }
                }


                foreach (var user in users)
                {
                    SqlCommand playlistCommand = connection.CreateCommand();
                    playlistCommand.CommandText =
@"SELECT * FROM Playlists AS p
INNER JOIN UserPlaylists AS up
ON p.PlaylistId = up.PlaylistId
WHERE up.UserId = @Id";
                    playlistCommand.AddParameter("@Id", user.Id);
                    SqlDataReader playlistReader = playlistCommand.ExecuteReader();

                    using (playlistReader)
                    {
                        while (playlistReader.Read())
                        {
                            user.Playlists.Add(
                                new Playlist(){
                                    Name = playlistReader["Name"].ToString(),
                                    IsPublic = (bool)playlistReader["IsPublic"]
                                });
                        }
                    }
                }

            }
            finally
            {
                connection.Close();
            }

            //var playlists = this.GetPlaylists();
            //this.GetUserPlaylists(users, playlists);

            return users;
        }

        public void Insert(User user)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText =
@"INSERT INTO Users (Name, Email, IsAdmin)
VALUES (@Name, @Email, @IsAdmin)";

                command
                    .AddParameter("@Name", user.Name)
                    .AddParameter("@Email", user.Email)
                    .AddParameter("@IsAdmin", user.IsAdmin);

                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public void Update(User user)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            

            try
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText =
    @"
UPDATE Users
SET Name = @Name,
Email = @Email,
IsAdmin = @IsAdmin
WHERE UserId = @Id
";
                command
                    .AddParameter("@Id", user.Id)
                    .AddParameter("@Name", user.Name)
                    .AddParameter("@Email", user.Email)
                    .AddParameter("@IsAdmin", user.IsAdmin);

                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }

        }

        public User Get(int id)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = connection.CreateCommand();
            command.CommandText =
@"SELECT * FROM Users WHERE UserId = @Id ";

            command.AddParameter("@Id", id);

            try
            {
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    User user = new User() {
                        Id = (int)reader["UserId"],
                        Name = reader["Name"].ToString(),
                        Email = reader["Email"].ToString(),
                        IsAdmin = (bool)reader["IsAdmin"]
                    };

                    return user;
                }
            }
            finally
            {
                connection.Close();
            }

            return null;
        }

        public void Delete(int id)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = connection.CreateCommand();
            command.CommandText =
@"DELETE FROM Users WHERE UserId = @Id";

            command.AddParameter("@Id", id);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        private List<Playlist> GetPlaylists()
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                List<Playlist> playlists = new List<Playlist>();
                SqlCommand playlistCommand = connection.CreateCommand();
                playlistCommand.CommandText = "SELECT * FROM Playlists";
                SqlDataReader playlistReader = playlistCommand.ExecuteReader();

                using (playlistReader)
                {
                    while (playlistReader.Read())
                    {
                        playlists.Add(new Playlist()
                        {
                            Id = (int)playlistReader["PlaylistId"],
                            IsPublic = (bool)playlistReader["IsPublic"],
                            Name = playlistReader["Name"].ToString()
                        });
                    }
                }

                return playlists;
            }
        }

        private void GetUserPlaylists(List<User> users, List<Playlist> playlists)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM UserPlaylists";
                SqlDataReader reader = command.ExecuteReader();

                using (reader)
                {
                    while (reader.Read())
                    {
                        int userId = (int)reader["UserId"];
                        int playlistId = (int)reader["PlaylistId"];

                        var user = users.Find(u => u.Id == userId);
                        var playlist = playlists.Find(p => p.Id == playlistId);

                        if (user != null && playlist != null)
                        {
                            user.Playlists.Add(playlist);
                        }
                    }
                }
            }
        }
    }
}