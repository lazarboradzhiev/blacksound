﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackSound.Data.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }

        public void PrintInfo()
        {
            foreach (var prop in this.GetType().GetProperties())
            {
                int playlistCounter = 1;
                var propValue = prop.GetValue(this, null);

                if (propValue is IEnumerable<BaseEntity>)
                {
                    IEnumerable<BaseEntity> items = propValue as IEnumerable<BaseEntity>;

                    foreach (var item in items)
                    {
                        Console.WriteLine("-----------------------------");
                        Console.WriteLine($"Playlist N{playlistCounter++}");
                        Console.WriteLine("-----------------------------");


                        foreach (var pr in item.GetType().GetProperties())
                        {
                            Console.WriteLine($"{pr.Name}: {pr.GetValue(item, null)}");

                        }
                        Console.WriteLine("-----------------------------");


                    }

                    //var proppyBoi = prop.GetType().GetProperties(); // kh
                }
                else
                {
                    Console.WriteLine($"{prop.Name}: {propValue}");
                }
            }
        }
    }
}
