﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackSound.Data.Entities
{
    public class Playlist : BaseEntity
    {
        public string Name { get; set; }

        public bool IsPublic { get; set; }
    }
}
