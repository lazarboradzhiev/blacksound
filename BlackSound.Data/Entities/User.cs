﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BlackSound.Data.Entities
{
    public class User : BaseEntity
    {
        public string Email { get; set; }

        public string Name { get; set; }

        public bool IsAdmin { get; set; }

        public List<Playlist> Playlists { get; set; } = new List<Playlist>();
    }
}