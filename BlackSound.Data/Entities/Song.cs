﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackSound.Data.Entities
{
    public class Song : BaseEntity
    {
        public string Title { get; set; }

        public string Author { get; set; }

        public string Year { get; set; }
    }
}
