﻿using BlackSound.Data.Entities;
using BlackSound.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlackSound
{
    class StartUp
    {
        static void Main(string[] args)
        {
            string connectionString = @"Server=.\SQLEXPRESS; Database=BlackSoundDb; Integrated Security=True";

            UserRepository userRepo = new UserRepository(connectionString);
            //userRepo.Insert(new User()
            //{
            //    Email = "s4f4ts@aad.com",
            //    Name = "asdafaef34",
            //    IsAdmin = false,
            //});

            //User user1 = (User)userRepo.Get(14);
            //user1.Name = "new name";
            //user1.Email = "new email";
            //user1.IsAdmin = true;

            //userRepo.Delete(14);

            List<User> users = userRepo.GetAll();

            foreach (var user in users)
            {
                user.PrintInfo();
                Console.WriteLine();
            }
        }
    }
}